FROM php:8.1.6-fpm

RUN apt-get update && apt-get install -y
RUN apt-get install nodejs npm -y
RUN apt-get install libxml2-dev libcurl4-openssl-dev libzip-dev zlib1g-dev -y
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install xml curl zip


RUN mkdir -p /usr/src/biletado
COPY . /usr/src/biletado
WORKDIR /usr/src/biletado
RUN npm install && composer install

CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
