<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', [\App\Http\Controllers\Auth\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\Auth\AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::get('/user', [\App\Http\Controllers\API\UsersController::class, 'show']);
    Route::get('/deleteUser/{name}', [\App\Http\Controllers\API\UsersController::class, 'delete']);
    Route::get('/logout', [\App\Http\Controllers\Auth\AuthController::class, 'logout']);

    Route::post('/updateName/{name}', [\App\Http\Controllers\API\UsersController::class, 'update']);
});

